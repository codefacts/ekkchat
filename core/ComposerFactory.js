import utils from '../utils/Utils.js';

function Composer(newMap, newCache) {
    var map = newMap || {};
    var cache = newCache || {};

    var $this = {

        get: function (componentName) {

          // console.log('#####' + componentName + ' requested');

          utils.requireNonNull(componentName, new Error('Composer: componentName can not be null'));

          if (!map[componentName]) {
              throw new Error("Composer: '" + componentName + "' does not exists");
          }

          if (!!cache[componentName]) {
              return cache[componentName];
          }

          var {factoryFunction, dependencies} = map[componentName];

          var dependencyComponents = Object.keys(dependencies).reduce((obj, dependencyName) => {

            try {
              // console.log('#####' + dependencyName + ' requested by ' + componentName);
              obj[dependencyName] = $this.get(dependencyName);
            } catch (e) {
              if (!!e.DependencyNotFoundError) {
                throw e;
              }
              console.error("Composer: error instantiating '" + dependencyName + "': " + (e && e.message), e);
              const ex = new Error("Composer: '" + dependencyName + "' does not exists requested by '" + componentName + "'");
              ex.DependencyNotFoundError = true;
              throw ex;
            }

            return obj;
          }, {});

          var newComponent = factoryFunction(dependencyComponents);

          if (newComponent === null || newComponent === undefined) {
              throw new Error("Can not export null or undefined, componentName: '" + componentName + "'");
          }

          return cache[componentName] = newComponent;

        },

        addDependencyInfo: function (componentName, dependencyInfo) {

          utils.requireNonNull(componentName);
          utils.requireNonNull(dependencyInfo);

          if (dependencyInfo.factoryFunction === null || dependencyInfo.factoryFunction === undefined) {
            throw new Error('factoryFunction is not defined');
          }

          if (dependencyInfo.dependencies === null || dependencyInfo.dependencies === undefined) {
            throw new Error('dependencies is not defined');
          }

          if (!!cache[componentName]) {
            throw new Error("component '" + componentName + "' is already in cache");
          }

          map[componentName] = dependencyInfo;

        },

        add: function (componentName, dependenciesArray, factoryFunction) {

          utils.requireNonNull(componentName, new Error('componentName can not be null'));
          utils.requireNonNull(factoryFunction, new Error('factoryFunction can not be null'));
          utils.requireNonNull(dependenciesArray, new Error('dependenciesArray can not be null'));

          if (!!cache[componentName]) {
            throw new Error("component '" + componentName + "' is already in cache");
          }

          var dependencies = toDependencyMap(dependenciesArray);

          map[componentName] = createDependencyInfo(factoryFunction, dependencies);

        },

        addAndReturnFactory: function (componentName, dependenciesArray, factoryFunction) {
          $this.add(componentName, dependenciesArray, factoryFunction);
          return factoryFunction;
        },

        updateWithDependencies: function (componentName, newDependencyArray, factoryFunction) {
          utils.requireNonNull(componentName, new Error('componentName is required'));
          utils.requireNonNull(factoryFunction, new Error('factoryFunction is required'));
          utils.requireNonNull(newDependencyArray, new Error('newDependencyArray is required'));

          var dependencies = toDependencyMap(newDependencyArray);

          updateDependencyInfo(componentName, dependencies, factoryFunction);
        },

        update: function (componentName, factoryFunction) {
          utils.requireNonNull(componentName, new Error('componentName is required'));
          utils.requireNonNull(factoryFunction, new Error('factoryFunction is required'));

          updateDependencyInfo(componentName, map[componentName].dependencies, factoryFunction);
        },

        exportTo: function (composer) {

          if (Object.keys(cache).length !== 0) {
            throw new Error("module is already dirty");
          }

          for(var componentName in map) {
            composer.add(componentName, map[componentName]);
          }
        },

        newInstance: function () {
          return new Composer(map, cache);
        }
    };

    function updateDependencyInfo(componentName, dependenciesMap, factoryFunction) {

      map[componentName] = createDependencyInfo(factoryFunction, dependenciesMap);

      invalidateCache(componentName);
    };

    function invalidateCache(componentName) {
      invalidateCacheRecursive(componentName, {});
    }

    function invalidateCacheRecursive(componentName, context) {

      if (!!context[componentName]) {
        return;
      }

      context[componentName] = true;

      if (!cache[componentName]) {
        return;
      }

      cache[componentName] = null;

      for(var parentComponentName in map) {

        if(!map[parentComponentName].dependencies[componentName]) {
          continue;
        }

        invalidateCacheRecursive(parentComponentName, context);
      }

    }

    return $this;
}

function createDependencyInfo(factoryFunction, dependencies) {
  return {
    factoryFunction: factoryFunction,
    dependencies: dependencies
  };
}

function toDependencyMap(dependenciesArray) {
  return dependenciesArray.reduce(function (map, dependencyName) {
    map[dependencyName] = true;
    return map;
  }, {});
}

export default Composer;
