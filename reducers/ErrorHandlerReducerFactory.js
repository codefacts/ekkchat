import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ErrorHandlerReducer',
[],
() => {
  return (errorMessages, action) => {
    const {type, id, message} = action;
    switch (type) {
      case 'pushError': {
        return errorMessages.concat([{id, message}]);
      }
      case 'deleteError': {
        return errorMessages.filter((item) => {
          return item.id !== id;
        });
      }
    }
    return errorMessages || [];
  };
});
