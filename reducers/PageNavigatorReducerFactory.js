import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('PageNavigatorReducer',
[],
() => {

  const navigator = (prevPage, message) => {
    const {page, type} = message;
    switch (type) {
      case 'navigateTo': {
        return page;
      }
    }
    return prevPage || null;
  };

  return navigator;
});
