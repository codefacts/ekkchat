import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ChangeHandlerReducers',
[],
() => {

  const onPhoneNumberChange = (oldPhone, message) => {
    const {phone, type} = message;
    switch (type) {
      case 'onPhoneNumberChange': {
        return phone || null;
      }
    }
    return oldPhone || null;
  };

  const onVerificationCodeChange = (oldVerificationCode, message) => {
    const {verificationCode, type} = message;
    switch (type) {
      case 'onVerificationCodeChange': {
        return verificationCode || null;
      }
    }
    return oldVerificationCode || null;
  };

  const onNameChange = (oldName, message) => {
    const {name, type} = message;
    switch (type) {
      case 'onNameChange': {
        return name || null;
      }
    }
    return oldName || null;
  };

  const onChatTextChange = (_chatText, message) => {
    const {type, chatText} = message;
    switch (type) {
      case 'onChatTextChange': {
        return chatText;
      }
    }
    return _chatText || null;
  };

  const setLoginName = (prevName, message) => {
    const {type, loginName} = message;
    switch (type) {
      case 'setLoginName': {
        return loginName;
      }
    }
    return prevName || null;
  };

  const setLoginUsername = (prevLoginUsername, message) => {
    const {type, loginUsername} = message;
    switch (type) {
      case 'setLoginUsername': {
        return loginUsername;
      }
    }
    return prevLoginUsername || null;
  };

  const setLoginPassword = (prevLoginPassword, message) => {
    const {type, loginPassword} = message;
    switch (type) {
      case 'setLoginPassword': {
        return loginPassword;
      }
    }
    return prevLoginPassword || null;
  };

  const setUsername = (prevUsername, message) => {
    const {type, username} = message;
    switch (type) {
      case 'setUsername': {
        return username;
      }
    }
    return prevUsername || null;
  };

  return {
    onPhoneNumberChange, onVerificationCodeChange, onNameChange, onChatTextChange,
    setLoginName, setLoginUsername, setLoginPassword, setUsername
  };
});
