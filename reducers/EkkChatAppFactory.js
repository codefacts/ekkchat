import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';
import { combineReducers } from 'redux';

composer.addAndReturnFactory('EkkChatApp',
['ChangeHandlerReducers', 'PageNavigatorReducer', 'ErrorHandlerReducer'],
({ChangeHandlerReducers, PageNavigatorReducer, ErrorHandlerReducer}) => {

  const {
    onPhoneNumberChange, onNameChange, onVerificationCodeChange, onChatTextChange,
    setLoginName, setLoginUsername, setLoginPassword, setUsername
  } = ChangeHandlerReducers;

  const oaisrokasdflas = (state, message) => {
    const filter = ['onPhoneNumberChange', 'onVerificationCodeChange', 'onNameChange', 'onChatTextChange'];
    if (filter.indexOf(message.type) !== -1) {
      return null;
    }
    console.log('### dispatching', message);
    return null;
  };

  const setId = (_id, message) => {
    const {type, id} = message;
    switch (type) {
      case 'setId': {
        u.requireNonNull(id);
        return id;
      }
    }
    return _id || null;
  };

  const setUserId = (oldUserId, message) => {
    const {type, userId} = message;
    switch (type) {
      case 'setUserId': {
        return userId;
      }
    }
    return oldUserId || null;
  };

  const selectPerson = (prevChattingTo, message) => {
    const {type, chatUser} = message;
    switch (type) {
      case 'selectPerson': {
        return chatUser;
      }
    }
    return prevChattingTo || null;
  };

  const addUsersToChatGrid = (prevChatUsers, message) => {
    const {users, type} = message;
    switch (type) {
      case 'addUsersToChatGrid': {

        const userIds = (prevChatUsers || []).reduce((idMap, user) => {
          idMap[user.userId] = true;
          return idMap;
        }, {});
        console.log('addUsersToChatGrid ==>> prevChatUsers', prevChatUsers);
        return prevChatUsers.concat(
          users.filter((item) => {
            return !userIds[item.userId];
          })
        );
      }
    }
    return prevChatUsers || [];
  };

  const addMessagesToChatGrid = (_msgs, action) => {
    _msgs = _msgs || [];
    const {type, messages} = action;
    switch (type) {
      case 'addMessagesToChatGrid': {
        return messages.concat(_msgs);
      }
    }
    return _msgs;
  };

  return combineReducers({
    phone: onPhoneNumberChange,
    name: onNameChange,
    verificationCode: onVerificationCodeChange,
    oaisrokasdflas,
    currentPage: PageNavigatorReducer,
    errorMessages: ErrorHandlerReducer,
    id: setId,
    userId: setUserId,
    chatUsers: addUsersToChatGrid,
    chattingTo: selectPerson,
    chatText: onChatTextChange,
    chatData: addMessagesToChatGrid,
    loginName: setLoginName,
    loginUsername: setLoginUsername,
    loginPassword: setLoginPassword,
    username: setUsername
  });
});
