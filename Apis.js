import composer from './core/Composer.js';

composer.addAndReturnFactory('Apis', [],
() => {
  return {
    loginApi: "/api/login",
    usersApi: '/api/users'
  };
});
