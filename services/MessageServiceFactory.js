import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('MessageService',
['Firebase'],
({Firebase}) => {
  const db = Firebase.firestore;

  const sendMessage = ({text, fromId, toId}) => {
    console.log('MessageService: sending ==>> ', {text, fromId, toId});
    return db.collection('users').doc(toId).collection('messages').add({
      text, fromId, toId, sentTimestamp: new Date().getTime()
    });
  };

  return {sendMessage};
});
