import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('UserAuthService',
['Firebase'],
({Firebase}) => {

  const {auth} = Firebase;

  const registerUser = ({username, password}) => {

    return new Promise((resolve, reject) => {
      return auth.createUserWithEmailAndPassword(username, password)
        .then(rsp => resolve(rsp))
        .catch((ex) => {
          reject(ex);
        });
    })
    .catch(ex => {
      console.error('#### UserAuthService: registerUser: ', ex);

      var message = ex.message;

      switch (ex.code) {
        case 'auth/argument-error': {
          message = "Invalid email! Please correct your email";
        }
        break;
        case 'auth/invalid-email': {
          message = "Invalid email! Please correct your email";
        }
        break;
        case 'auth/email-already-in-use': {
          message = 'Email already taken! Please provide a new one';
        }
        break;
        case 'auth/weak-password': {
          message = 'Password is too weak';
        }
      }

      return Promise.reject({message});
    });
  };

  const loginUser = (username, password) => {

    return new Promise((resolve, reject) => {
      auth.signInWithEmailAndPassword(username, password)
        .then(({email}) => {
          console.log('UserAuthService: loginUser success ==>> ', email);
          resolve(email);
        })
        .catch((ex) => reject(ex));
    })
    .catch(ex => {
      console.error('UserAuthService: loginUser => ', ex);
      var message = ex.message;
      switch (ex.code) {
        case 'auth/argument-error': {
          message = "Email is not valid";
        }
        break;
        case 'auth/invalid-email': {
          message = "Email is not valid";
        }
        break;
        case 'auth/user-not-found': {
          message = "User not found";
        }
        break;
        case 'auth/wrong-password': {
          message = "Incorrect password";
        }
        break;
      }
      return Promise.reject({message});
    });
  };

  return {registerUser, loginUser};
});
