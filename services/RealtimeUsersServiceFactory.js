import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('RealtimeUsersService',
['Firebase', 'ErrorActions', 'UserService', 'EkkHelpers'],
({Firebase, ErrorActions, UserService, EkkHelpers}) => {

  const db = Firebase.firestore;
  const {notifyError} = ErrorActions;
  const {toMessage} = EkkHelpers;

  var unsubscribe = null;

  const start = (dispatch, {context, addUsersToChatGrid}) => {
    UserService.findAll()
      .then((users) => {
        users = users.filter((item) => item.userId !== context.userId);
        dispatch(addUsersToChatGrid(users));
      })
      .then(() => {
        listenToNewUser(dispatch, {context, addUsersToChatGrid});
      })
      .catch(ex => {
        dispatch(
          notifyError(
            toMessage(ex)
          )
        );
      });
  };

  const listenToNewUser = (dispatch, {context, addUsersToChatGrid}) => {
    unsubscribe = db.collection("users")
    .onSnapshot((querySnapshot) => {
      console.log('RealtimeUsersService-Users', querySnapshot.docs);
      const users = querySnapshot.docs.map((doc) => Object.assign({}, doc.data(), {id: doc.id}))
        .filter((_user) => (_user.userId !== context.userId));
      dispatch(addUsersToChatGrid(users));
    }, ex => console.error(ex));
  };

  const stop = (dispatch, context) => {
    unsubscribe();
  };

  return {start, stop};
});
