import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('PhoneVerificationService',
['Firebase'],
({Firebase}) => {

  const {firebase, firestore} = Firebase;
  const recapctaMountPoint = 'ekk-recaptcha-verifier';

  var confirmationResult = null;

  const sendVerificationCode = ({phone}) => {

    return Promise.resolve('sendVerificationCode success');

    // return new Promise((resolve, reject) => {
    //   const recaptchaVerifier = new firebase.auth.RecaptchaVerifier(recapctaMountPoint, {
    //       'size': 'normal',
    //       'callback': function(response) {
    //       },
    //       'expired-callback': function() {
    //         console.error(new Error('recaptcha time expired'));
    //       }
    //     });
    //     resolve(recaptchaVerifier);
    //   })
    //   .then((recaptchaVerifier) => {
    //     return firebase.auth().signInWithPhoneNumber(phone, recaptchaVerifier);
    //   })
    //   .then((_confirmationResult) => {
    //     console.log('################# confirmationResult', confirmationResult);
    //     confirmationResult = _confirmationResult;
    //   })
    //   .catch((ex) => {
    //     console.error('### error', ex);
    //     return Promise.reject(ex);
    //   });
  };

  const verifyCode = ({verificationCode}) => {
    return Promise.resolve('verifyCode success');
    // return new Promise((resolve, reject) => {
    //   u.requireNonNull(confirmationResult, new Error('confirmationResult is null'));
    //   return confirmationResult.confirm(verificationCode)
    //     .then(resolve, reject);
    // })
    // .catch((ex) => {
    //   console.error('### error', ex);
    //   return Promise.reject(ex);
    // });
  };

  return {sendVerificationCode, verifyCode};
});
