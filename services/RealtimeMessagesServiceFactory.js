import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('RealtimeMessagesService',
['Firebase'],
({Firebase}) => {
  const db = Firebase.firestore;

  var unsubscribe = null;

  const start = (dispatch, {context, addMessagesToChatGrid}) => {

    const chattingTo = context.chattingTo;

    if (!chattingTo) {
      return;
    }

    console.log('RealtimeMessagesService: starting...', context);

    unsubscribe = db.collection("users").doc(context.id).collection('messages')
      .where('fromId', '==', chattingTo.id)
      .orderBy('sentTimestamp', 'desc')
      .limit(2)
      .onSnapshot((querySnapshot) => {
        var messages = [];
        querySnapshot.docChanges.forEach(({type, doc}) => {
          if (type !== 'added') {
            return;
          }
          console.log('### message-received:', doc.data());
          messages.push(Object.assign({}, doc.data(), {
            id: doc.id,
            messageType: 'received'
          }));
        });
        dispatch(
          addMessagesToChatGrid(messages)
        );
      }, ex => {
        console.error(ex);
      });

    // db.collection("users").doc("SF")
    // .onSnapshot(function(doc) {
    //     console.log("Current data: ", doc && doc.data());
    // });
  };

  const stop = (dispatch, context) => {
    unsubscribe && unsubscribe();
  };

  return {start, stop};
});
