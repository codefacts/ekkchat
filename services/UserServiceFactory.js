import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('UserService',
['Firebase'],
({Firebase}) => {

  const {firebase, createFirebase, makeFirestore} = Firebase;

  const newId = () => (Math.random() + '-' + Math.random() + '-' + Math.random());

  const findByUsername = (username) => {

    // console.warn('[[findByUsername]]');
    // return makeFirestore().collection("test_stores").add({name: 'Kmol Vaia'})
    //     .then((docRef) => {
    //         console.warn("Document written with ID: " + docRef.id);
    //     })
    //     .catch((error) => {
    //         console.error("Error adding document: ", error);
    //         console.warn("Error adding document: " + error.message);
    //     });

    return firebase.firestore().collection("users")
      .where('username', '==', username)
      .get()
      .then((querySnapshot) => {
        console.log('findByPhone-querySnapshot', querySnapshot.docs, username);
        if (querySnapshot.empty) {
          // alert('findByUsername querySnapshot.empty');
          const exx = new Error("userNotFound");
          exx.errorCode = 'userNotFound';
          return Promise.reject(exx);
        }
        const user = Object.assign(querySnapshot.docs[0].data(), {id: querySnapshot.docs[0].id});
        console.log('findByPhone-User: ', user);
        return u.except(user, ['password']);
      })
      .catch((ex) => {
        console.error(ex);
        return Promise.reject(ex);
      });
  };

  const addUser = (user) => {

    if (!user.name) {
      return Promise.reject("Name is required");
    }

    user = u.except(user, ['password']);

    const userId = newId();
    user = Object.assign(user, {userId});
    return firebase.firestore().collection("users").add(user)
    .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
        console.log('new User Created with userId = ' + userId, user);
        return Object.assign(user, {id: docRef.id});
    })
    .catch((error) => {
        console.error("Error adding document: ", error);
        return Promise.reject(error);
    });
  };

  const findAll = () => {
    return firebase.firestore().collection("users")
    .get()
    .then(function(querySnapshot) {
        return querySnapshot.docs.map((doc) => u.except(
          Object.assign({}, doc.data(), {id: doc.id}), ['password']
        ));
    })
    .then(users => {
      console.log('######## ++++++------ users', users);
      return users;
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
        return Promise.reject(error);
    });
  };

  return {findByUsername, addUser, findAll};
});
