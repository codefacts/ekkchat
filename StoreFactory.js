import composer from './core/Composer.js';
import u from './utils/Utils.js';

import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

composer.addAndReturnFactory('Store',
['EkkChatApp'],
({EkkChatApp}) => {

  let store = createStore(EkkChatApp, applyMiddleware(
    thunkMiddleware
  ));
  console.warn('#####[[store created]]#####');

  return store;
});
