import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  FlatList,
  TouchableOpacity
} from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ChatBoxView',
['ChatGridView', 'RxChatUserListView', 'ChatBoxActions', 'Store'],
({ChatGridView, RxChatUserListView, ChatBoxActions, Store}) => {

  const {onChatBoxOpened, onChatBoxClosed} = ChatBoxActions;

  class ChatBoxView extends Component {

    constructor(props) {
       super(props);
       this.state = {
       };
    }

    componentDidMount() {
      Store.dispatch(
        onChatBoxOpened(this.props.context)
      );
    }

    componentWillUnmount() {
      Store.dispatch(
        onChatBoxClosed(this.props.context)
      );
    }

    render () {
      const props = this.props;
      const {name, chatText, chatData, context} = props;
      const {onSendMessage, onChatTextChange, onShowChatUsers} = props;

      return (
        <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

          <Button
            style={{flex: 1}}
            onPress={() => onShowChatUsers()}
            title="Select User To Chat"
            color="#841584"
          />

          <ChatGridView chatData={chatData} style={{flex: 10}}/>

          <TextInput
            style={{flex: 1, borderColor: 'gray', borderWidth: 1}}
            onChangeText={(text) => onChatTextChange(text, context)}
            value={chatText || ''}
            multiline={false}
          />

          <Button
            style={{flex: 1, marginBottom: 10}}
            onPress={() => onSendMessage(chatText, context)}
            title="Send"
            color="#841584"
          />

        </View>
      );
    }

  };

  return ChatBoxView;
});
