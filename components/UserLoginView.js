import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('UserLoginView',
[],
() => {

  const UserLoginView = (props) => {

    const {username, password} = props;
    const {onUsernameChange, onPasswordChange, onUserLogin, onRegister} = props;

    return (
      <View>
        <Text>Username:</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1, width: 150}}
          onChangeText={(text) => onUsernameChange(text)}
          value={username || ''}
        />
        <Text>Password:</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1, width: 150}}
          onChangeText={(text) => onPasswordChange(text)}
          value={password || ''}
        />
        <Button
          onPress={() => onUserLogin({username, password})}
          title="Login"
          color="#841584"
        />
        <Text>Don't have an account?</Text>
        <Button
          onPress={() => onRegister()}
          title="Register"
          color="#841584"
        />
      </View>
    );
  };

  return UserLoginView;
});
