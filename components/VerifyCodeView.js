import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('VerifyCodeView',
[],
() => {

  const VerifyCodeView = (props) => {

    const {verificationCode, context} = props;
    const {onVerificationCodeEnter, onVerificationCodeChange} = props;

    return (
      <View>
        <Text>A Verification code is sent to your phone number. Please type in the code</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => onVerificationCodeChange(text, context)}
          value={verificationCode}
        />
        <Button
          onPress={() => onVerificationCodeEnter(verificationCode, context)}
          title="Verify"
          color="#841584"
        />
      </View>
    );
  };

  return VerifyCodeView;
});
