import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('RegisterUserView',
[],
() => {

  const RegisterUserView = (props) => {

    const {name, username, password} = props;
    const {
      onNameChange, onUsernameChange,
      onPasswordChange, onUserRegister
    } = props;

    return (
      <View>
        <Text>Name:</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1, width: 150}}
          onChangeText={(text) => onNameChange(text)}
          value={name || ''}
        />
        <Text>Username:</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1, width: 150}}
          onChangeText={(text) => onUsernameChange(text)}
          value={username || ''}
        />
        <Text>Password:</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1, width: 150}}
          onChangeText={(text) => onPasswordChange(text)}
          value={password || ''}
        />
        <Button
          onPress={() => onUserRegister({name, username, password})}
          title="Register"
          color="#841584"
        />
      </View>
    );
  };

  return RegisterUserView;
});
