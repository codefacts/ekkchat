import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  FlatList,
  TouchableOpacity
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ChatUserListView',
['Store', 'ChatUserListActions'],
({Store, ChatUserListActions}) => {

  const {onChatUserListOpened, onChatUserListClosed} = ChatUserListActions;

  const keyExtractor = (item, index) => ('' + index);

  class ChatUserList extends Component {

    constructor(props) {
       super(props);
       this.state = {
       };
    }

    componentDidMount() {
      Store.dispatch(
        onChatUserListOpened(this.props.context)
      );
    }

    componentWillUnmount() {
      Store.dispatch(
        onChatUserListClosed(this.props.context)
      );
    }

    render () {
      const props = this.props;
      const {chatUsers} = props;
      const {onChatUserSelect} = props;

      const renderItem = ({item}) => (
        <TouchableOpacity onPress={() => onChatUserSelect(item)}>
          <View style={{borderColor: '#4a4136', borderWidth: 1, marginBottom: 10}}>
            <Text>{item.name}</Text>
            <Text>{item.username}</Text>
          </View>
        </TouchableOpacity>
      );

      return (
        <FlatList
          keyExtractor={keyExtractor}
          data={chatUsers}
          renderItem={renderItem}
        />
      );
    }
  };

  return ChatUserList;
});
