import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('NameInputView',
[],
() => {

  const NameInputView = (props) => {

    const {name} = props;
    const {onNameEnter, onNameChange, context} = props;

    return (
      <View>
        <Text>Please type in your name</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => onNameChange(text, context)}
          value={name}
        />
        <Button
          onPress={() => onNameEnter(name, context)}
          title="Submit"
          color="#841584"
        />
      </View>
    );
  };

  return NameInputView;
});
