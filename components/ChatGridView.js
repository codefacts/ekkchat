import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  FlatList,
  TouchableOpacity
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ChatGridView',
[],
() => {

  const ChatGridView = (props) => {
    const {chatData, style} = props;

    const keyExtractor = (item, index) => ('' + index);

    const renderItem = ({item}) => {
      const {text, messageType} = item;
      return (
        <Text style={{flex: 1, textAlign: (messageType === 'received' ? 'left' : 'right')}}>{text}</Text>
      );
    };

    return (
      <View style={style}>
        <FlatList
          keyExtractor={keyExtractor}
          data={(chatData || []).slice().reverse()}
          renderItem={renderItem}
        />
      </View>
    );
  };

  return ChatGridView;
});
