import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('SendCodeView',
[],
() => {

  const SendCodeView = (props) => {

    const {phone} = props;
    const {onPhoneNumberEnter, onPhoneNumberChange} = props;

    return (
      <View>
        <Text>Please Type in Your Phone Number</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => onPhoneNumberChange(text)}
          value={phone}
        />
        <Button
          onPress={() => onPhoneNumberEnter(phone)}
          title="Send Verification Code"
          color="#841584"
        />
      </View>
    );
  };

  return SendCodeView;
});
