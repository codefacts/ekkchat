import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  FlatList,
  TouchableOpacity
} from 'react-native';

import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('EkkChatAppView',
[
  'RxSendCodeView', 'RxVerifyCodeView', 'RxNameInputView', 'RxChatBoxView',
  'RxChatUserListView', 'RxUserLoginView', 'RxRegisterUserView'
],
(ViewMap) => {

  const {RxSendCodeView} = ViewMap;

  const ErrorGridView = (props) => {
    const {errorMessages, style} = props;
    return (
      <View id="ErrorGridView" style={style}>
        {
          (errorMessages || []).map(({message}, index) => (
            <Text key={'msg-' + index}>{message}</Text>
          ))
        }
      </View>
    );
  };

  return (props) => {
    const {currentPage, name, username, chattingTo, errorMessages} = props;
    const {onSignOut} = props;

    const PageToShow = ViewMap[currentPage] || ViewMap.RxUserLoginView;

    return (
      <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

        <Text style={{flex: 10}}>
          {(name && (name + ':' + username)) || ''}
          {(!!name && !!chattingTo) ? ('\n=>' + chattingTo.name + ':' + chattingTo.username) : ''}
        </Text>

        {
          !(!!errorMessages && !!errorMessages.length) ? null : (<ErrorGridView style={{flex: 10}} errorMessages={errorMessages}/>)
        }

        <View style={{flex: 70}}>
          <PageToShow />
        </View>

      </View>
    );
  };
});
