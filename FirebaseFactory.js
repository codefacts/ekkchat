import composer from './core/Composer.js';
import u from './utils/Utils.js';

export default composer.addAndReturnFactory('Firebase',
[],
() => {

  const createFirebase = () => {
    var config = {
      apiKey: "AIzaSyAZ4HeUhOxBWT7NOyW19RwZZMnF3g9DtTA",
      authDomain: "ekkpass.firebaseapp.com",
      databaseURL: "https://ekkpass.firebaseio.com",
      projectId: "ekkpass",
      storageBucket: "",
      messagingSenderId: "405632696982"
    };

    const firebase = require("firebase");
    // Required for side-effects
    require("firebase/auth");
    require("firebase/firestore");

    firebase.initializeApp(config);
    return firebase;
  };

  const firebase = createFirebase();

  return {
    firebase,
    firestore: firebase.firestore(),
    auth: firebase.auth(),
    createFirebase,
    makeFirestore: () => firebase.firestore() 
  };
});
