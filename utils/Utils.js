var _ = require('lodash');

const copy = (obj) => {
  if (obj === null | obj === undefined) {
    return obj;
  }
  return JSON.parse(JSON.stringify(obj));
};

const throwError = (ex) => {

  if (ex === null || ex === undefined) {
    throw new Error('Value is required');
  }

  if (typeof ex === 'string') {
    throw new Error(ex);
  }
  throw ex;
};

var $this = {
  copy,
  except: (obj, array) => _.omit(obj, array),

  isPlainObject: _.isPlainObject,
  isArray: _.isArray,
  isError: _.isError,
  includeOnly: (obj, includingFields) => {
    obj = obj || {};
    var newObj = {};
    (includingFields || []).forEach((field) => {
      if (obj.hasOwnProperty(field)) {
        newObj[field] = obj[field];
      }
    });
    return newObj;
  },

  filterOutNulls: (obj) => {
    var cc = {};
    for (var key in obj) {
      var val = obj[key];
      if (val === null || val === undefined) {
        continue;
      }
      cc[key] = val;
    }
    return cc;
  },

  filterOutEmptyNulls: (obj) => {
    var cc = {};
    for (var key in obj) {
      var val = obj[key];
      if (val === null || val === undefined || val === '') {
        continue;
      }
      cc[key] = val;
    }
    return cc;
  },

  toUrlPath: function () {

    var parts = [];

    for(var i = 0; i < arguments.length; i++) {

      var pp = arguments[i];

      pp = pp.startsWith('/') ? pp.substr(1) : pp;
      pp = pp.endsWith('/') ? pp.substr(0, pp.length - 1) : pp;

      if (pp === '' || pp === '/') {
        continue;
      }

      parts.push(pp);
    }

    var uri = parts.join('/');

    uri = arguments[0].startsWith('/') ? ('/' + uri) : uri;

    if (uri !== '/') {
      uri = arguments[arguments.length - 1].endsWith('/') ? (uri + '/') : uri;
    }

    // console.log('uri', uri);
    return uri;
  },

  requireNonNull: function (obj, error) {
    if (obj === null || obj === undefined) {
      throwError(error);
    }
    return obj;
  },

  copyObject: function (obj) {
    var dst = {};
    for(var key in obj) {
      dst[key] = obj[key];
    }
    return dst;
  },
  merge: function (obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16) {

    var args = [obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16];

    if (args.length === 0) {
      throw new Error("merge requires atleast 2 args");
    }

    // $this.requireNonNull(args[0]);

    var obj = {};

    for(var i = 0; i < args.length; i++) {
      obj = mergeTwo(obj, args[i] || {});
    }

    return obj;
  },
  requireNonEmpty: (value, error) => {
    if (value === null || value === undefined || value === '') {
      throwError(error);
    }
    return value;
  },
  mergeWithNull: function (obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16) {

    var args = [obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15, obj16];

    if (args.length === 0) {
      throw new Error("merge requires atleast 2 args");
    }

    // $this.requireNonNull(args[0]);

    var obj = {};

    for(var i = 0; i < args.length; i++) {
      obj = mergeWithNullTwo(obj, args[i] || {});
    }

    return obj;
  },
  formatDate: (date) => {
    if (date === null || date === undefined) {
      return "";
    }
    const year = date.getFullYear() + '';
    var mm = (date.getMonth() + 1) + '';
    var dd = date.getDate() + '';
    mm = ((mm.length === 1) ? ('0' + (mm + '')) : mm);
    dd = ((dd.length === 1) ? ('0' + (dd + '')) : dd);
    return year + '-' + mm + '-' + dd;
  },

  formatTime: function (date) {
    const hour = date.getHours();
    const ampm = hour >= 12 ? 'PM' : 'AM';
    const hr = (hour > 12 ? hour - 12 : hour === 0 ? 12 : hour) + '';

    const min = date.getMinutes() + '';
    const seconds = date.getSeconds() + '';

    return (hr.length === 1 ? ('0' + hr) : hr) + ":" + (min.length === 1 ? ('0' + min) : min) + ':' + (seconds.length === 1 ? ('0' + seconds) : seconds) + ' ' + ampm;
  },

  parseDate: (dateStr) => {

    if (dateStr === null || dateStr === undefined || dateStr === '') {
      return "";
    }

    var splits = dateStr.split('-');
    var year = parseInt(splits[0]);
    var month = parseInt(splits[1] || 1);
    var date = parseInt(splits[2] || 1);

    return new Date(year, month - 1, date);
  },

  parseTime: (date, timeStr) => {

    $this.requireNonNull(date);

    if (timeStr === null || timeStr === undefined || timeStr === '') {
      return date;
    }

    var splits = timeStr.split(':');
    const hour = parseInt(splits[0] || 0);
    const minute = parseInt(splits[1] || 0);

    var secPart = splits[2] || '';
    const secPartSplits = secPart.split('.');

    const seconds = parseInt(secPartSplits[0] || 0);
    const millis = parseInt(secPartSplits[1] || 0);

    return new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      hour,
      minute,
      seconds,
      millis
    );
  },

  fileNameExtension: (name)  => {
      var index = name.lastIndexOf('.');
      if (index < 0) {
          return "";
      }

      return name.length > (index + 1) ? name.substr(index + 1) : "";
  },
  deleteArrayElementAt: (array, index) => {
    array = array.slice();
    array.splice(index, 1);
    return array;
  }
};

window.utils = $this;
window._ = _;

module.exports = $this;

window.mergeTwo = function mergeTwo(obj1, obj2) {
  for (var key in obj2) {
    var oldVal = obj1[key];
    var val = obj2[key];

    if (val === null || val === undefined) {
      continue;
    }

    if (!!_.isPlainObject(oldVal) && !!_.isPlainObject(val)) {
      const oldVal22 = copyJust11(oldVal);
      val = mergeTwo(oldVal22, val);
    }

    obj1[key] = val;
  }

  return obj1;
}

window.mergeWithNullTwo = function mergeWithNullTwo(obj1, obj2) {
  for (var key in obj2) {
    var oldVal = obj1[key];
    var val = obj2[key];

    if (!!_.isPlainObject(oldVal) && !!_.isPlainObject(val)) {
      const oldVal22 = copyJust11(oldVal);
      val = mergeWithNullTwo(oldVal22, val);
    }

    obj1[key] = val;
  }

  return obj1;
}

const copyJust11 = (oldVal) => {
  const obj = {};
  for (const key in oldVal) {
    obj[key] = oldVal[key];
  }
  return obj;
};
