import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('EkkHelpers',
[],
() => {
  const toMessage = (err) => {
    console.error('toMessage error ===>>> ', err);
    return !!err ? err.message : 'Unexpected Error';
  };

  return {toMessage};
});
