import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ChangeActions',
[],
() => {

  const setId = (id) => {
    return {
      type: 'setId',
      id
    };
  };

  const onPhoneNumberChange = (newPhoneNum) => {
    return {
      type: 'onPhoneNumberChange',
      phone: newPhoneNum
    };
  };

  const onVerificationCodeChange = (newVerificationCode) => {
    return {
      type: 'onVerificationCodeChange',
      verificationCode: newVerificationCode
    };
  };

  const onNameChange = (newName) => {
    return {
      type: 'onNameChange',
      name: newName
    };
  };

  const onChatTextChange = (newChatText) => {
    return {
      type: 'onChatTextChange',
      chatText: newChatText
    };
  };

  const setUserId = (userId) => {
    return {
      type: 'setUserId',
      userId
    };
  };

  const setLoginName = (loginName) => {
    return {
      type: 'setLoginName',
      loginName
    };
  };

  const setLoginUsername = (username) => {
    return {
      type: 'setLoginUsername',
      loginUsername: username
    };
  };

  const setLoginPassword = (password) => {
    return {
      type: 'setLoginPassword',
      loginPassword: password
    };
  };

  const setUsername = (username) => {
    return {
      type: 'setUsername',
      username
    };
  };

  return {
    onPhoneNumberChange, onVerificationCodeChange,
    onNameChange, onChatTextChange,
    setUserId, setId, setLoginName,
    setLoginUsername, setLoginPassword,
    setUsername
  };
});
