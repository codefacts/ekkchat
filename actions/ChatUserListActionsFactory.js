import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ChatUserListActions',
['RealtimeUsersService', 'Actions'],
({RealtimeUsersService, Actions}) => {

  const {navigateTo} = Actions;

  const onChatUserListOpened = (context) => {
    return (dispatch) => {
      RealtimeUsersService.start(dispatch, {context, addUsersToChatGrid});
    };
  };

  const onChatUserListClosed = (context) => {
    return (dispatch) => {
      RealtimeUsersService.stop(dispatch, context);
    };
  };

  const onChatUserSelect = (chatUser) => {
    return (dispatch) => {
      dispatch(
        selectPerson(chatUser)
      );
      dispatch(
        navigateTo('RxChatBoxView')
      );
    };
  };

  const selectPerson = (chatUser) => {
    u.requireNonNull(chatUser.id, new Error('Id is required for chat user'));
    return {
      type: 'selectPerson',
      chatUser
    };
  };

  const addUsersToChatGrid = (users) => {
    return {
      type: 'addUsersToChatGrid',
      users
    };
  };

  return {
    selectPerson, addUsersToChatGrid, onChatUserSelect,
    onChatUserListOpened, onChatUserListClosed
  };
});
