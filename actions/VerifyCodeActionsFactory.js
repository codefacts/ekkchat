import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('VerifyCodeActions',
['ErrorActions', 'Actions', 'EkkHelpers', 'PhoneVerificationService', 'UserService', 'SetCurrentUser'],
({ErrorActions, Actions, EkkHelpers, PhoneVerificationService, UserService, SetCurrentUser}) => {

  const {toMessage} = EkkHelpers;
  const {notifyError} = ErrorActions;
  const {navigateTo} = Actions;

  const verifyCode = (verificationCode, {phone}) => {
    return (dispatch) => {
      PhoneVerificationService.verifyCode({verificationCode})
        .then(user => {
          console.log('############################### verificationCode success', user, phone);
          dispatch(
            registerUser(phone)
          );
        })
        .catch((err) => {
          dispatch(
            notifyError(toMessage(err))
          );
        });
    };
  };

  const resendCode = (phone) => {
    return {
      type: 'resendCode',
      phone
    };
  };

  const registerUser = (phone) => {

    return (dispatch) => {
      UserService.findByPhone(phone)
        .then(user => {

          SetCurrentUser(dispatch, user);

          dispatch(
            navigateTo('RxChatBoxView')
          );
        })
        .catch(ex => {
          console.error('registerUser Error', ex);
          if (ex.errorCode === 'userNotFound') {
            dispatch(
              navigateTo('RxNameInputView')
            );
            return;
          }
          notifyError(toMessage(ex));
        });
    };
  };

  return {
    verifyCode, resendCode
  };
});
