import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('RegisterUserActions',
['EkkHelpers', 'ErrorActions', 'Actions', 'UserAuthService', 'UserService'],
({EkkHelpers, ErrorActions, Actions, UserAuthService, UserService}) => {

  const {toMessage} = EkkHelpers;
  const {notifyError} = ErrorActions;
  const {navigateTo} = Actions;

  const registerUser = ({loginName, loginUsername, loginPassword}) => {

    return (dispatch) => {

      const user = {
        name: loginName,
        username: loginUsername,
        password: loginPassword
      };

      console.log('RegisterUserActions: registerUser: ', user);

      UserAuthService.registerUser(user)
        .then(() => {
          return UserService.addUser(user);
        })
        .then(user => {
          dispatch(navigateTo('RxUserLoginView'));
        })
        .catch(ex => {
          console.error('RegisterUserActions: ', ex);
          dispatch(notifyError((ex && ex.message) || 'Unknown Error'));
        });
    };
  };

  return {
    registerUser
  };
});
