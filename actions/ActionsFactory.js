import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('Actions',
['EkkHelpers'],
({EkkHelpers}) => {

  const {toMessage} = EkkHelpers;

  const navigateTo = (nextPage) => {
    return {
      type: 'navigateTo',
      page: nextPage
    };
  };

  const signOut = () => {
    return {
      type: 'signOut'
    };
  };

  return {
    navigateTo, signOut
  };
});
