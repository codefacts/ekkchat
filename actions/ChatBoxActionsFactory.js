import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ChatBoxActions',
['RealtimeMessagesService', 'EkkHelpers', 'MessageService', 'ErrorActions', 'ChangeActions'],
({RealtimeMessagesService, EkkHelpers, MessageService, ErrorActions, ChangeActions}) => {

  const {toMessage} = EkkHelpers;
  const {notifyError} = ErrorActions;
  const {onChatTextChange} = ChangeActions;

  const onChatBoxOpened = (context) => {
    return (dispatch) => {
      RealtimeMessagesService.start(dispatch, {context, addMessagesToChatGrid});
    };
  };

  const onChatBoxClosed = (context) => {
    return (dispatch) => {
      RealtimeMessagesService.stop(dispatch, context);
    };
  };

  const addMessagesToChatGrid = (messages) => {
    return {
      type: 'addMessagesToChatGrid',
      messages
    };
  };

  const sendMessage = (text, context) => {
    u.requireNonNull(context, new Error('context is required'));
    return (dispatch) => {

      dispatch(
        addMessagesToChatGrid([{text, messageType: 'sent'}])
      );
      dispatch(
        onChatTextChange('')
      );

      MessageService.sendMessage({
        text, fromId: context.id, toId: context.chattingTo.id
      })
        .then(rsp => {
          console.log('messageSent: ', text);
        })
        .catch(ex => dispatch(
          notifyError(toMessage(ex))
        ));
      };
    };

  return {
    onChatBoxOpened, onChatBoxClosed,
    addMessagesToChatGrid, sendMessage
  };
});
