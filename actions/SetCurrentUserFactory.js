import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('SetCurrentUser',
['ChangeActions'],
({ChangeActions}) => {

  const {setId, setUserId, onNameChange, setUsername} = ChangeActions;

  const setCurrentUser = (dispatch, {id, userId, name, username}) => {

    u.requireNonNull(id, new Error('Id is required'));
    u.requireNonNull(name, new Error('Name is required'));
    u.requireNonNull(username, new Error('Username is required'));

    dispatch(setId(id));
    dispatch(setUserId(userId));
    dispatch(onNameChange(name));
    dispatch(setUsername(username));
  };

  return setCurrentUser;
});
