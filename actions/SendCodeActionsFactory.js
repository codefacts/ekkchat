import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('SendCodeActions',
['ErrorActions', 'Actions', 'EkkHelpers', 'PhoneVerificationService'],
({ErrorActions, Actions, EkkHelpers, PhoneVerificationService}) => {

  const {toMessage} = EkkHelpers;
  const {notifyError} = ErrorActions;
  const {navigateTo} = Actions;

  const sendCode = (phone) => {
    return (dispatch) => {

      PhoneVerificationService.sendVerificationCode({phone})
        .then(() => {
          dispatch(navigateTo('RxVerifyCodeView'));
        })
        .catch(ex => {
          dispatch(notifyError(
            toMessage(ex)
          ));
        });
    };
  };

  return {
    sendCode
  };
});
