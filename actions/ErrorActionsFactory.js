import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('ErrorActions',
[],
() => {

  const pushError = (error) => {
    return Object.assign(error, {
      type: 'pushError'
    });
  };

  const deleteError = (errorMessageId) => {
    return {
      type: 'deleteError',
      id: errorMessageId
    };
  };

  const notifyError = (message) => {
    return (dispatch) => {

      const id = Math.random() + '-' + Math.random() + '-' + Math.random();

      dispatch(pushError(
        {id, message}
      ));

      setTimeout(() => {
        dispatch(deleteError(id));
      }, 10000);
    };
  };

  return {
    pushError, deleteError, notifyError
  };
});
