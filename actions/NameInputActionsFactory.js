import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('NameInputActions',
['ErrorActions', 'Actions', 'EkkHelpers', 'SetCurrentUser', 'UserService'],
({ErrorActions, Actions, EkkHelpers, SetCurrentUser, UserService}) => {

  const {toMessage} = EkkHelpers;
  const {notifyError} = ErrorActions;
  const {navigateTo} = Actions;

  const onNameEnter = (name, {phone}) => {
    return (dispatch) => {
      UserService.addUser({name, phone})
        .then(user => {

          SetCurrentUser(dispatch, user);

          dispatch(
            navigateTo('RxChatBoxView')
          );
        })
        .catch(ex => {
          dispatch(notifyError(toMessage(ex)));
        });
    };
  };

  return {onNameEnter};
});
