import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

composer.addAndReturnFactory('UserLoginActions',
['EkkHelpers', 'Firebase', 'ErrorActions', 'Actions', 'UserAuthService', 'UserService', 'SetCurrentUser'],
({EkkHelpers, Firebase, ErrorActions, Actions, UserAuthService, UserService, SetCurrentUser}) => {

  const {toMessage} = EkkHelpers;
  const {notifyError} = ErrorActions;
  const {navigateTo} = Actions;
  const {auth} = Firebase;

  const loginUser = ({loginUsername, loginPassword}) => {
    return (dispatch) => {

      console.log('UserLoginActions: loginUser: ', {loginUsername, loginPassword});

      UserAuthService.loginUser(loginUsername, loginPassword)
        .then(email => {
          return UserService.findByUsername(email);
        })
        .then((user) => {
          console.log('UserLoginActions: loginUser: user found ==>>', user);
          SetCurrentUser(dispatch, user);
          dispatch(navigateTo('RxChatBoxView'));
        })
        .catch(ex => {
          dispatch(notifyError(
            (ex && ex.message) || 'Unknown Error'
          ));
        });
    };
  };

  return {
    loginUser
  };
});
