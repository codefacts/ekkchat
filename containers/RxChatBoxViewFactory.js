import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';
import { connect } from 'react-redux';

composer.addAndReturnFactory('RxChatBoxView',
['ChatBoxView', 'ChatBoxActions', 'ChangeActions', 'Actions'],
({ChatBoxView, ChatBoxActions, ChangeActions, Actions}) => {

  const {sendMessage} = ChatBoxActions;
  const {onChatTextChange} = ChangeActions;
  const {navigateTo} = Actions;

  const mapStateToProps = (state) => {
    const {id, name, userId, chattingTo, chatText, chatData} = state;
    console.log('RxChatBoxView: mapStateToProps ==>> ', state);
    return {
      name, chattingTo, chatText, chatData,
      context: {id, name, userId, chattingTo}
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      onChatTextChange: (chatText) => dispatch(
        onChatTextChange(chatText)
      ),
      onSendMessage: (chatText, context) => {
        dispatch(
          sendMessage(chatText, context)
        );
      },
      onShowChatUsers: () => dispatch(
        navigateTo('RxChatUserListView')
      )
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(ChatBoxView);
});
