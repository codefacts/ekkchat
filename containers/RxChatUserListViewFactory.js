import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';
import { connect } from 'react-redux';

composer.addAndReturnFactory('RxChatUserListView',
['ChatUserListView', 'ChatUserListActions'],
({ChatUserListView, ChatUserListActions}) => {

  const {onChatUserSelect} = ChatUserListActions;

  const mapStateToProps = ({id, name, userId, username, chatUsers}) => {
    return {
      chatUsers,
      context: {id, name, userId, username}
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      onChatUserSelect: (chatUser) => dispatch(onChatUserSelect(chatUser))
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(ChatUserListView);
});
