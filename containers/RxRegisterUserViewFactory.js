import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';
import { connect } from 'react-redux';

composer.addAndReturnFactory('RxRegisterUserView',
['RegisterUserView', 'ChangeActions', 'RegisterUserActions'],
({RegisterUserView, ChangeActions, RegisterUserActions}) => {

  const {setLoginName, setLoginUsername, setLoginPassword} = ChangeActions;
  const {registerUser} = RegisterUserActions;

  const mapStateToProps = ({loginName, loginUsername, loginPassword}) => {
    return {
      name: loginName,
      username: loginUsername,
      password: loginPassword,
      context: {name: loginName, loginUsername, loginPassword}
    };
  };

  const mapDispatchToProps = (dispatch) => {
    return {
      onNameChange: (newName) => dispatch(setLoginName(newName)),
      onUsernameChange: (username) => dispatch(setLoginUsername(username)),
      onPasswordChange: (password) => dispatch(setLoginPassword(password)),
      onUserRegister: ({name, username, password}) => dispatch(registerUser({
        loginName: name,
        loginUsername: username,
        loginPassword: password
      }))
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(RegisterUserView);
});
