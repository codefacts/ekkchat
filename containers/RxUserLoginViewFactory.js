import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

import { connect } from 'react-redux';

composer.addAndReturnFactory('RxUserLoginView',
['UserLoginView', 'ChangeActions', 'UserLoginActions', 'Actions'],
({UserLoginView, ChangeActions, UserLoginActions, Actions}) => {

  const {setLoginUsername, setLoginPassword} = ChangeActions;
  const {loginUser} = UserLoginActions;
  const {navigateTo} = Actions;

  u.requireNonNull(setLoginUsername, new Error('setLoginUsername is required'));
  u.requireNonNull(setLoginPassword, new Error('setLoginPassword is required'));
  u.requireNonNull(loginUser, new Error('loginUser is required'));

  const mapStateToProps = ({loginUsername, loginPassword}) => {
    return {
      username: loginUsername,
      password: loginPassword
    };
  };

  const mapDispatchToProps = (dispatch) => {
    return {
      onUsernameChange: (newUsername) => dispatch(setLoginUsername(newUsername)),
      onPasswordChange: (newPassword) => dispatch(setLoginPassword(newPassword)),
      onRegister: () => dispatch(navigateTo('RxRegisterUserView')),
      onUserLogin: ({username, password}) => dispatch(loginUser({
        loginUsername: username,
        loginPassword: password
      }))
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserLoginView);
});
