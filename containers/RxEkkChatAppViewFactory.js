import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';
import { connect } from 'react-redux';

composer.addAndReturnFactory('RxEkkChatAppView',
['EkkChatAppView'],
({EkkChatAppView}) => {

  const mapStateToProps = ({currentPage, errorMessages, name, username, chattingTo}) => {
    console.log('RxEkkChatAppView: state ==>> ', {currentPage, errorMessages, name, username, chattingTo});
    return {
      currentPage,
      errorMessages,
      name,
      username,
      chattingTo
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(EkkChatAppView);
});
