import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';

import { connect } from 'react-redux';

composer.addAndReturnFactory('RxSendCodeView',
['SendCodeView', 'ChangeActions', 'SendCodeActions'],
({SendCodeView, ChangeActions, SendCodeActions}) => {

  const {onPhoneNumberChange} = ChangeActions;
  const {sendCode} = SendCodeActions;

  const mapStateToProps = ({phone}) => {
    return {
      phone
    };
  };

  const mapDispatchToProps = (dispatch) => {
    return {
      onPhoneNumberChange: (newPhoneNum) => dispatch(onPhoneNumberChange(newPhoneNum)),
      onPhoneNumberEnter: (phone) => dispatch(sendCode(phone))
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(SendCodeView);
});
