import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';
import { connect } from 'react-redux';

composer.addAndReturnFactory('RxNameInputView',
['NameInputView', 'ChangeActions', 'NameInputActions'],
({NameInputView, ChangeActions, NameInputActions}) => {

  const {onNameChange} = ChangeActions;
  const {onNameEnter} = NameInputActions;

  const mapStateToProps = ({name, phone}) => {
    return {
      name,
      context: {phone}
    }
  };

  const mapDispatchToProps = dispatch => {
    return {
      onNameChange: (newName, {phone}) => dispatch(onNameChange(newName, {phone})),
      onNameEnter: (newName, {phone}) => dispatch(onNameEnter(newName, {phone}))
    }
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(NameInputView);
});
