import React, { Component } from 'react';
import composer from '../core/Composer.js';
import u from '../utils/Utils.js';
import { connect } from 'react-redux';

composer.addAndReturnFactory('RxVerifyCodeView',
['VerifyCodeView', 'ChangeActions', 'VerifyCodeActions'],
({VerifyCodeView, ChangeActions, VerifyCodeActions}) => {

  const {onVerificationCodeChange} = ChangeActions;
  const {verifyCode} = VerifyCodeActions;

  const mapStateToProps = (state) => {
    const {verificationCode, phone} = state;
    console.log('RxVerifyCodeView phone', phone, state);
    return {
      verificationCode,
      context: {phone}
    }
  };

  const mapDispatchToProps = (dispatch) => {
    return {
      onVerificationCodeChange: (verificationCode) => dispatch(onVerificationCodeChange(verificationCode)),
      onVerificationCodeEnter: (verificationCode, {phone}) => dispatch(verifyCode(verificationCode, {phone}))
    };
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(VerifyCodeView);
});
